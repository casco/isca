baseline
postLoadActions
	"Start a Server on 8080 and Setup Repository"

	GTPlayground
		edit:
			'"To register the interactive agent application evaluate:"

(WAAdmin register:  ISCALandingComponent  asApplicationAt: ''isca'')
      addLibrary: JQDeploymentLibrary;
      addLibrary:  TBSDeploymentLibrary. 	
WAAdmin register: ISCAApi at: ''isca-api''.

"To configure the server evaluate:"
ISCAMboxComponent ServerURL: ''http://localhost:8080/isca-api/agents/''.

"To start the interactive agent server run"
ZnZincServerAdaptor startOn: 8080.

"To register the node inspector application evaluate:"
(WAAdmin register:  SSCSLandingComponent  asApplicationAt: ''sscs'')
      addLibrary: JQDeploymentLibrary;
      addLibrary:  TBSDeploymentLibrary

"If communication is not working check the existense of:

protocol: *Seaside-Pharo20-REST-Core

argumentNamesOf: aCompiledMethod
	^ aCompiledMethod argumentNames

"
'
		label: 'Tycho'