baseline
baseline: spec
	<baseline>
	spec
		for: #common
		do: [ self
				seaside: spec;
				bootstrap: spec;
				bootstrapSkeleton: spec;
				highlightJs: spec;
				seasideUtilities: spec;
				neoJSON: spec.
			spec
				package: 'ISCA'
				with: [ spec
						requires:
							#('Seaside3' 'Seaside-REST-Core' 'Bootstrap-Core' 'Bootstrap-Widgets' 'NeoJSON' 'Bootstrap-Skeleton' 'HighlightJs' 'Seaside-Utilities') ].
			spec
				package: 'SSCS'
				with: [ spec
						requires:
							#('Seaside3' 'Seaside-REST-Core' 'Bootstrap-Core' 'Bootstrap-Widgets' 'NeoJSON' 'Bootstrap-Skeleton' 'HighlightJs' 'Seaside-Utilities') ].
			spec postLoadDoIt: #postLoadActions ]