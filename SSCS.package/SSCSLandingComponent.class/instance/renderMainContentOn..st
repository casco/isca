rendering
renderMainContentOn: html
	html
		tbsJumbotron: [ html heading: self applicationName.
			html tbsForm
				with: [ html
						tbsFormGroup: [ html label: 'URI base for the simulation node'.
							html textInput
								on: #baseUrl of: self;
								tbsFormControl;
								id: 'baseUrl';
								placeholder: 'http://localhost:3000' ].
					html
						tbsFormGroup: [ html label: 'Provide your access code'.
							html textInput
								on: #accessCode of: self;
								tbsFormControl;
								id: 'accessCode';
								placeholder: 'your access code' ].
					html tbsButton
						callback: [ self grantAccessOnValidCode ];
						with: 'Submit' ] ]