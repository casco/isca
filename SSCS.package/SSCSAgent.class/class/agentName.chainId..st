instance creation
agentName: aString chainId: anInteger
	^ self new
		agentName: aString;
		chainId: anInteger;
		yourself