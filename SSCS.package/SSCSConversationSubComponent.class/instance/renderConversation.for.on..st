rendering
renderConversation: aConversation for: aComponent on: html
	html tbsPanel beDefault
		with: [ self renderConversationHeadingFor: aConversation on: html.
			html
				tbsPanelBody: [ html heading
						level4;
						with: 'Summary'.
					self renderConversationSummaryFor: aConversation on: html.
					html heading
						level4;
						with: [ html text: 'Messages '.
							html space.
							self renderCollapseControlsFor: aConversation on: html ].
					(uncollapsedConversations includes: aConversation id) not
						ifFalse: [ threaded
								ifTrue: [ self render: aConversation threadedOn: html ]
								ifFalse: [ self render: aConversation chronologicalOn: html ] ] ] ]