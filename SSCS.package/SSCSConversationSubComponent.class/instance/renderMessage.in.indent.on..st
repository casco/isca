rendering
renderMessage: aMessage in: aConversation indent: indent on: html
	html tbsColumn
		mediumSize: 6;
		mediumOffset: indent;
		with: [ html tbsPanel beDefault; class: aMessage performative;
			 with: [ self renderPropertiesOf: aMessage On: html ] ].
	(aConversation repliesTo: aMessage)
		do: [ :aReply | 
			self
				renderMessage: aReply
				in: aConversation
				indent: indent + 1
				on: html ]