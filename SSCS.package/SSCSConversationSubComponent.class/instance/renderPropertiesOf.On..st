rendering
renderPropertiesOf: aMessage On: html
	^ html
		tbsPanelBody: [ html
				strong: 'Sender: ';
				text: aMessage sender;
				break.
			html
				strong: 'Receiver: ';
				text: aMessage receiver;
				break.
			html
				strong: 'Performative: ';
				text: aMessage performative;
				break.
			html
				strong: 'In reply to: ';
				text: aMessage inReplyTo;
				break.
			html
				strong: 'Reply with: ';
				text: aMessage replyWith;
				break.
			html
				strong: 'Content: ';
				break.
			DictionaryRenderer new render: aMessage content for: nil on: html ]