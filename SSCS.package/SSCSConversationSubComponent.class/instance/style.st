style
style
	^ '.CallForProposal { 
  background-color: white;
}

.Propose { 
  background-color: lightgray;
}

.AcceptProposal { 
  background-color: green;
}

.RejectProposal { 
  background-color: red;
}

.InformDone { 
  background-color: lightgray;
}

.RejectProposal { 
  background-color: red;
}

.Propose { 
  background-color: lightgreen;
}'