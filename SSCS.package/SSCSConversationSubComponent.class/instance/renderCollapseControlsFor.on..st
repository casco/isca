rendering
renderCollapseControlsFor: aConversation on: html
	(uncollapsedConversations includes: aConversation id) not
		ifFalse: [ html anchor
				callback: [ self collapse: aConversation ];
				with: [ html tbsGlyphIcon iconCollapseUp ].
			html space.
			html tbsGlyphIcon iconCollapseDown ]
		ifTrue: [ html tbsGlyphIcon iconCollapseUp.
			html space.
			html anchor
				callback: [ self uncollapse: aConversation ];
				with: [ html tbsGlyphIcon iconCollapseDown ] ]