rendering
renderConversationSummaryFor: aConversation on: html
	| bagOfMessageTypes |
	html
		paragraph: [ html
				strong: 'Total of messages: ';
				text: aConversation messages size asString ].
	bagOfMessageTypes := (aConversation messages collect: [ :each | each performative ]) asBag.
	html
		paragraph: [ bagOfMessageTypes asSet
				do: [ :type | 
					html
						strong: type , ': ';
						text: (bagOfMessageTypes occurrencesOf: type) asString ]
				separatedBy: [ html text: ' - ' ] ].
	html
		strong: 'Total accepted amount: ';
		text: aConversation totalAcceptedAmount asString