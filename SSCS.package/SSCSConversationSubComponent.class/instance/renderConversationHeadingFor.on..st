rendering
renderConversationHeadingFor: aConversation on: html
	html
		tbsPanelHeading: [ html strong: 'Conversation ' , aConversation id asString.
			(uncollapsedConversations includes: aConversation id)
				ifTrue: [ html div
						tbsPullRight;
						with: [ html text: 'Sort by: '.
							threaded
								ifTrue: [ html text: 'thread'.
									html text: ' - '.
									html anchor
										callback: [ threaded := false ];
										with: 'time' ]
								ifFalse: [ html anchor
										callback: [ threaded := true ];
										with: 'thread'.
									html text: ' - '.
									html text: 'time' ] ] ] ]