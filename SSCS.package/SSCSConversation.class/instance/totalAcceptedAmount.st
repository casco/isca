statistics
totalAcceptedAmount
	^ (messages select: [ :each | each performative = 'InformDone' ]) sumNumbers: [ :each | each content at: 'price' ]