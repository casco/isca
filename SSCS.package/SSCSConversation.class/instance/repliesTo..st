accessing
repliesTo: aMessage
	^ messages select: [ :each | each inReplyTo notNil & (each inReplyTo = aMessage replyWith) ]