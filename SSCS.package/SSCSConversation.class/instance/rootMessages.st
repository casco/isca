accessing
rootMessages
	^ messages select: [ :each | each inReplyTo isNil ]