rendering
renderContentOn: html
	html tbsPanel beDefault
		with: [ html
				tbsPanelHeading:
					[ html tbsPanelTitle: 'Details of simulation node at: ' , self baseUrl ].
			html
				tbsPanelBody: [ html heading
						level4;
						with: 'Collections in this node'.
					html
						unorderedList: [ self chains
								do: [ :each | 
									html
										listItem: [ html anchor
												callback: [ self toggleChainSelectionFor: each ];
												with: each chainName ] ] ].
					html anchor
						callback: [ self addNewChain ];
						with: [ html tbsButton
								beExtraSmall;
								with: 'Add new' ].
					html space.
					html anchor
						callback: [ self loadExamples ];
						with: [ html tbsButton
								beExtraSmall;
								with: 'Add examples' ] ] ]