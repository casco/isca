rendering
renderEditableProduct: product on: html
	html
		text: product;
		space.
	^ html anchor
		callback: [ self removeSuppliedProduct: product ];
		with: [ html tbsGlyphIcon iconTrash ]