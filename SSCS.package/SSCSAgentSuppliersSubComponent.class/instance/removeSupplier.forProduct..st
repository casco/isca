callbacks
removeSupplier: id forProduct: product
	| agent |
	agent := self agent.
	agent suppliers at: product put: ((agent suppliers at: product) copyWithout: id).
	self save: agent