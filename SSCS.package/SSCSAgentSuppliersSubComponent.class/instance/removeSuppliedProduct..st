callbacks
removeSuppliedProduct: product
	| agent |
	agent := self agent.
	agent suppliers removeKey: product.
	self save: agent