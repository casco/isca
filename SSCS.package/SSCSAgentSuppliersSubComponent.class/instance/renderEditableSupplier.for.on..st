rendering
renderEditableSupplier: id for: product on: html
	^ html
		listItem: [ html
				text: id;
				space.
			html anchor
				callback: [ self removeSupplier: id forProduct: product ];
				with: [ html tbsGlyphIcon iconTrash ] ]