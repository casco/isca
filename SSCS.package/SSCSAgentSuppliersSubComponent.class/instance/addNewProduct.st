callbacks
addNewProduct
	| agent |
	nameOfNewProduct notEmpty
		ifTrue: [ agent := self agent.
			agent suppliers at: nameOfNewProduct put: Array new.
			self save: agent.
			nameOfNewProduct := '' ]