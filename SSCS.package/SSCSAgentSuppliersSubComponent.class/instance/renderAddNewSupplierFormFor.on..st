rendering
renderAddNewSupplierFormFor: aProduct on: html
	html
		tbsForm: [ html textInput
				on: #nameOfNewSupplier of: self;
				size: 80.
			html space.
			html tbsButton
				beDefault;
				beExtraSmall;
				callback: [ self addNewSupplierFor: aProduct ];
				with: 'Add as new supplier' ]