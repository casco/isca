callbacks
addNewSupplierFor: aProduct
	| agent |
	nameOfNewSupplier notEmpty
		ifTrue: [ agent := self agent.
			agent suppliers at: aProduct put: ((agent suppliers at: aProduct) copyWith: nameOfNewSupplier).
			self save: agent.
			nameOfNewSupplier := '' ]