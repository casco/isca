rendering
renderEditingOn: html
	html heading
		level4;
		with: 'Suppliers'.
	html
		unorderedList: [ self agent suppliers keys
				do: [ :product | 
					html listItem: [ self renderEditableProduct: product on: html ].
					html
						unorderedList: [ (self agent suppliers at: product) do: [ :id | self renderEditableSupplier: id for: product on: html ].
							self renderAddNewSupplierFormFor: product on: html ] ] ].
	self renderAddNewProductFormOn: html.
	html break.
	self renderCancelEditButtonOn: html