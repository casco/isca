rendering
renderAddNewProductFormOn: html
	html
		tbsForm: [ html textInput on: #nameOfNewProduct of: self.
			html space.
			html tbsButton
				beDefault;
				beExtraSmall;
				callback: [ self addNewProduct ];
				with: 'Add as new product' ]