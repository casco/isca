rendering
renderNotEditingOn: html
	html heading
		level4;
		with: [ html text: 'Suppliers'.
			html space.
			html anchor
				callback: [ editing := true ];
				with: [ html tbsGlyphIcon iconPencil ] ].
	html
		unorderedList: [ self agent suppliers keys
				do: [ :product | 
					html listItem: product.
					html unorderedList: [ (self agent suppliers at: product) do: [ :id | html listItem: id ] ] ] ]