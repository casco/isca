accessing
toggleChainSelectionFor: aChain
	selectedChainId
		ifNil: [ selectedChainId := aChain id ]
		ifNotNil: [ selectedChainId = aChain id
				ifTrue: [ selectedChainId := nil ]
				ifFalse: [ selectedChainId := aChain id ] ].
	cachedSelectedChain := nil