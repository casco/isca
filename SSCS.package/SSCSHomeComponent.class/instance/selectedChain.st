accessing
selectedChain
	^ cachedSelectedChain ifNil: [ cachedSelectedChain := self api chain: selectedChainId ]