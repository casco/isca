accessing
userStore
	^ String
		streamContents: [ :stream | 
			(NeoJSONWriter on: stream)
				prettyPrint: true;
				nextPut: cachedAgent userStore ]