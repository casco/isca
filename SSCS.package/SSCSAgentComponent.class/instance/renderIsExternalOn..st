rendering-view
renderIsExternalOn: html
	html heading
		level4;
		with: 'Location'.
	^ html
		paragraph:
			(self agent is_external
				ifTrue: [ 'External' ]
				ifFalse: [ 'This node' ])