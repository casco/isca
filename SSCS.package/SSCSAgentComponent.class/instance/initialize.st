initialization
initialize
	super initialize.
	editingAgentType := false.
	editingLocation := false.
	privateDataEditor := HighlightJsComponent new.
	suppliersSubcomponent := SSCSAgentSuppliersSubComponent parent: self.
	conversationsSubcomponent := SSCSConversationSubComponent parent: self.
	agentNameEditorSubcomponent := InPlacePropertyEditorComponent on: #agentName of: self.
	agentNameEditorSubcomponent label: 'Agent name'