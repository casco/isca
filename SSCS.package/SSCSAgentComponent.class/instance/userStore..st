callbacks
userStore: aString
	| agent |
	agent := api agent: agentId.
	agent userStore: (NeoJSONReader fromString: aString).
	self save: agent