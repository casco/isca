rendering-view
renderAgentTypeOn: html
	| menu |
	html heading
		level4;
		with: [ html text: 'Agent type'.
			html space.
			html anchor
				callback: [ editingAgentType := true ];
				with: [ html tbsGlyphIcon iconPencil ] ].
	editingAgentType
		ifTrue: [ html tbsAlert
				beWarning;
				with: [ html strong: 'Warning!'.
					html
						text: ' If you change the agent''s type, its private data will be reset to the new type''s dfault.' ].
			menu := [ api agentTypes
				do: [ :type | 
					html
						tbsDropdownMenuItem: [ html anchor
								callback: [ self changeClassOfAgentTo: type ];
								with: (type at: 'class') ] ].
			html tbsDivider.
			html
				tbsDropdownMenuItem: [ html anchor
						callback: [ editingAgentType := false ];
						with: 'Cancel' ] ].
			html
				tbsButtonGroup: [ html tbsDropdownButton
						beDefault;
						with: [ html
								text: self agent agentType;
								tbsCaret ].
					html
						tbsDropdownMenu: menu;
						space ] ]
		ifFalse: [ html paragraph: self agent agentType ]