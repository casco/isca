callbacks
changeClassOfAgentTo: aTypeDictionary
	| agent |
	agent := api agent: agentId.
	agent agentType: (aTypeDictionary at: 'class').
	agent userStore: (aTypeDictionary at: 'storeAttribute').
	editingAgentType := false.
	self save: agent