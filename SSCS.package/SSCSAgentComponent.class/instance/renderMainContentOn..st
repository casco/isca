rendering
renderMainContentOn: html
	html tbsPanel beDefault
		with: [ html tbsPanelHeading: [ html tbsPanelTitle: [ html text: 'Details of agent ' , self agent agentName ] ].
			html
				tbsPanelBody: [ 
					html render: agentNameEditorSubcomponent .
					self renderUriOn: html.
					"self renderIsExternalOn: html."
					self agent is_external
						ifFalse: [ html render: suppliersSubcomponent.
							self
								renderAgentTypeOn: html;
								renderUserStoreOn: html;
								renderConversationsOn: html ] ] ].
	cachedAgent := nil