accessing
agent
	^ cachedAgent ifNil: [ cachedAgent := api agent: agentId ]