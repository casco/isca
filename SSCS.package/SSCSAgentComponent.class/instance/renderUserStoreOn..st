rendering-view
renderUserStoreOn: html
	html heading
		level4;
		with: [ html text: 'Private data'.
			html space.
			html anchor
				callback: [ privateDataEditor editing: true ];
				with: [ html tbsGlyphIcon iconPencil ] ].
	privateDataEditor on: #userStore of: self.
	html render: privateDataEditor

	"DictionaryRenderer new render: self agent userStore for: self on: html"