callbacks
agentName: aName
	| agent |
	agent := self agent.
	agent agentName: aName.
	self save: agent