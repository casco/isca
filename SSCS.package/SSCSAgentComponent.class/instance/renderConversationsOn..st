rendering-view
renderConversationsOn: html
	html heading
		level4;
		with: 'Conversations'.
	conversationsSubcomponent conversations: self agent conversations.
	html render: conversationsSubcomponent