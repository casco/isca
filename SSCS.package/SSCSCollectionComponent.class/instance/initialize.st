initialization
initialize
	super initialize.
	nameEditor := InPlacePropertyEditorComponent on: #chainName of: self.
	nameEditor label: 'Collection name'.
	descriptionEditor := InPlacePropertyEditorComponent on: #chainDescription of: self.
	descriptionEditor label: 'Collection description'.
	descriptionEditor beTextArea.
	conversationSubComponent := SSCSConversationSubComponent parent: self. 