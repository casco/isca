accessing
chainDescription: aString
	| chain |
	chain := self parent selectedChain.
	chain description: aString.
	self parent saveChain: chain. 