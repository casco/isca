redering
renderAgentListOn: html
	html
		unorderedList: [ self parent selectedChain agents
				do: [ :each | 
					html
						listItem: [ html anchor
								callback: [ self parent browseAgent: each ];
								with: each agentName ] ] ].
	html anchor
		callback: [ self addNewAgent ];
		with: [ html tbsButton
				beExtraSmall;
				with: 'Add new' ]