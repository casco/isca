accessing
chainName: aString
	| chain |
	chain := self parent selectedChain.
	chain chainName: aString.
	self parent saveChain: chain. 