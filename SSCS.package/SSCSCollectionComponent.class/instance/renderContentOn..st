redering
renderContentOn: html
	html tbsPanel beDefault
		with: [ html tbsPanelHeading: [ html tbsPanelTitle: 'Details of collection: ' , self parent selectedChain chainName ].
			html
				tbsPanelBody: [ html render: nameEditor.
					html render: descriptionEditor.
					html heading
						level4;
						with: 'Agents in this collection'.
					self renderAgentListOn: html.
					html heading
						level4;
						with: 'Past conversations in this collection'.
					self renderChainConversationsOn: html. ] ]