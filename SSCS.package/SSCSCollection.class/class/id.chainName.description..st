instance creation
id: anId chainName: aName description: someText
	^ self new
		id: anId;
		chainName: aName;
		description: someText;
		yourself