rendering
renderKey: aString value: aValueOrDictionary on: html
	aValueOrDictionary isDictionary
		ifFalse: [ html listItem: aString , ': ' , aValueOrDictionary asString ]
		ifTrue: [ html listItem: aString.
			html unorderedList: [ aValueOrDictionary keysAndValuesDo: [ :key :value | self renderKey: key value: value on: html ] ] ]