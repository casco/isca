querying
agentTypes
	"(ServerlessEndpointAPI baseUrl: 'http://localhost:3000') agentTypes"

	self log: thisContext method selector.
	^ NeoJSONReader fromString: (ZnClient new get: baseUrl , '/agents/subclassesOfAgents')