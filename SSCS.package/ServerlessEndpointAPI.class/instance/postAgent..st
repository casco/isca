saving
postAgent: aNewlyCreatedAgent
	self log: thisContext method selector.
	^ ZnClient new
		post: baseUrl , '/agents'
		contents:
			(String
				streamContents: [ :stream | 
					(NeoJSONWriter on: stream)
						mapInstVarsFor: SSCSAgent;
						nextPut: aNewlyCreatedAgent ])