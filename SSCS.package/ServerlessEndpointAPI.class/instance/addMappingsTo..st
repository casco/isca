neojson-mappings
addMappingsTo: reader
	| mapping |
	reader mapInstVarsFor: ISCAMessage.
	reader for: #ArrayOfISCAMessage customDo: [ :map | map listOfElementSchema: ISCAMessage ].
	mapping := reader mapInstVarsFor: SSCSConversation.
	(mapping mapInstVar: #messages) valueSchema: #ArrayOfISCAMessage.
	reader for: #ArrayOfConversation customDo: [ :map | map listOfElementSchema: SSCSConversation ].
	mapping := reader mapInstVarsFor: SSCSAgent.
	(mapping mapInstVar: #conversations) valueSchema: #ArrayOfConversation.
	reader for: #ArrayOfAgent customDo: [ :map | map listOfElementSchema: SSCSAgent ].
	mapping := reader mapInstVarsFor: SSCSCollection.
	(mapping mapInstVar: #agents) valueSchema: #ArrayOfAgent.
	reader for: #ArrayOfChain customDo: [ :map | map listOfElementSchema: SSCSCollection ]