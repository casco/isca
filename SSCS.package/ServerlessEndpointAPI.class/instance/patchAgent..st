saving
patchAgent: anAgent
	self log: thisContext method selector.
	^ ZnClient new
		patch: anAgent uri
		contents:
			(String
				streamContents: [ :stream | 
					(NeoJSONWriter on: stream)
						mapInstVarsFor: SSCSAgent;
						nextPut: anAgent ])