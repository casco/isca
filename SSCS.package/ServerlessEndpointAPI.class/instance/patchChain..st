saving
patchChain: aChain
	self log: thisContext method selector.
	aChain
		agents: nil;
		transactionNumber: nil.
	^ ZnClient new
		patch: baseUrl , '/chains/' , aChain id printString
		contents:
			(String
				streamContents: [ :stream | 
					(NeoJSONWriter on: stream)
						mapInstVarsFor: SSCSCollection;
						nextPut: aChain ])