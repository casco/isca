querying
chains
	"(ServerlessEndpointAPI baseUrl: 'http://localhost:3000') chains"

	| reader |
	self log: thisContext method selector.
	reader := NeoJSONReader on: (ZnClient new get: baseUrl , '/chains') readStream.
	self addMappingsTo: reader.
	^ reader nextAs: #ArrayOfChain