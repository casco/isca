querying
conversation: conversationId forChain: chainId
	"(ServerlessEndpointAPI baseUrl: 'http://localhost:3000') conversation: 59 forChain: 8005623435559399"

	| reader |
	self log: thisContext method selector.
	reader := NeoJSONReader
		on:
			(ZnClient new get: baseUrl , '/chains/' , chainId asString , '/conversations/' , conversationId asString)
				readStream.
	self addMappingsTo: reader.
	^ SSCSConversation id: conversationId messages: (reader nextAs: #ArrayOfISCAMessage)