querying
agent: id
	"(ServerlessEndpointAPI baseUrl: 'http://localhost:3000') agent: '8006130933530660'"

	| reader |
	self log: thisContext method selector.
	reader := NeoJSONReader on: (ZnClient new get: baseUrl , '/agents/' , id printString) readStream.
	self addMappingsTo: reader.
	^ reader nextAs: SSCSAgent