querying
chain: id
	"(ServerlessEndpointAPI baseUrl: 'http://localhost:3000') chain: '8005623435559399'"

	| reader |
	self log: thisContext method selector.
	reader := NeoJSONReader on: (ZnClient new get: baseUrl , '/chains/' , id asString) readStream.
	self addMappingsTo: reader.
	^ reader nextAs: SSCSCollection