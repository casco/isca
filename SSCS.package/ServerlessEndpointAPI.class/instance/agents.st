querying
agents
	"(ServerlessEndpointAPI baseUrl: 'http://localhost:3000') agents"

	| reader |
	self log: thisContext method selector.
	reader := NeoJSONReader on: (ZnClient new get: baseUrl , '/agents') readStream.
	self addMappingsTo: reader.
	^ reader nextAs: #ArrayOfAgents