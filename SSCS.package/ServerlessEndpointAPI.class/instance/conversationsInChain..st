querying
conversationsInChain: id
	"(ServerlessEndpointAPI baseUrl: 'http://localhost:3000') conversationsInChain: 8005623435559399"

	| conversationIds request |
	self log: thisContext method selector.
	request := ZnClient new url: baseUrl , '/chains/' , id printString , '/conversations'.
	request get.
	request response isError ifTrue: [ ^Set new ].
	conversationIds := NeoJSONReader
		fromString: request response contents.
	^ conversationIds collect: [ :each | self conversation: each forChain: id ]