saving
postChain: aNewlyCreatedChain
	self log: thisContext method selector.
	^ ZnClient new
		post: baseUrl , '/chains'
		contents:
			(String
				streamContents: [ :stream | 
					(NeoJSONWriter on: stream)
						mapInstVarsFor: SSCSCollection;
						nextPut: aNewlyCreatedChain ])