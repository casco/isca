rendering
renderMessage: msg on: html
	html
		tableRow: [ html
				tableData: [ html anchor
						callback: [ self view: msg ];
						with: [ msg read
								ifTrue: [ html text: (self headerForMessage: msg) ]
								ifFalse: [ html strong: (self headerForMessage: msg) ] ] ];
				tableData: [ self renderActionButtonsFor: msg on: html ] ]