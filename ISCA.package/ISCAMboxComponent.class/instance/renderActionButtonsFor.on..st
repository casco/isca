rendering
renderActionButtonsFor: msg on: html
	html anchor
		callback: [ self delete: msg ];
		with: [ html tbsButton
				beExtraSmall;
				with: 'delete' ].
	html space.
	msg class = CallForProposal
		ifTrue: [ self renderActionButtonsForCallForProposal: msg on: html ].
	msg class = Propose
		ifTrue: [ self renderActionButtonsForPropose: msg on: html ].
	msg class = RejectProposal
		ifTrue: [ self renderActionButtonsForReject: msg on: html ]