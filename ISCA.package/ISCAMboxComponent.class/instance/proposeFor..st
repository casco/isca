callbacks
proposeFor: aCallForProposal 
	| propose |
	propose := aCallForProposal asProposeTemplate .
	self
		call:
			((FlexibleComposer  model: propose)
				agent: self model;
				yourself)