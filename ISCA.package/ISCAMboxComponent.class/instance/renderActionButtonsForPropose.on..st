rendering
renderActionButtonsForPropose: msg on: html
	msg sender = self model url
		ifFalse: [ html anchor
				callback: [ self rejectForPropose: msg ];
				with: [ html tbsButton
						beExtraSmall;
						with: 'reject' ].
			html space.
			html anchor
				callback: [ self acceptForPropose: msg ];
				with: [ html tbsButton
						beExtraSmall;
						with: 'accept' ] ]