callbacks
composeCallForProposals
	| cfp |
	cfp := self model newCallForProposals.
	self
		call:
			((CFPComposer   model: cfp)
				agent: self model;
				yourself)