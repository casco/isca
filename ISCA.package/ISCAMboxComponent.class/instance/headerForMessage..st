rendering
headerForMessage: msg
	^ msg sender = self model url
		ifTrue: [ 'To ' , msg receiver , ' (' , msg class name , ')' ]
		ifFalse: [ 'From ' , msg sender , ' (' , msg class name , ')' ]