rendering
renderMessagesOn: html
	html
		tbsTable: [ html
				tableHead: [ html
						tableHeading: 'Messages';
						tableHeading: 'Actions' ].
			html
				tableBody: [ self model conversations reversed
						do: [ :collectionOfMessages | 
							self renderMessage: collectionOfMessages first indent: 0 on: html.
							(collectionOfMessages copyWithout: collectionOfMessages first)
								do: [ :each | self renderMessage: each indent: 1 on: html ] ] ] ]