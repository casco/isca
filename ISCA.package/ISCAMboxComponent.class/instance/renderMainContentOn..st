rendering
renderMainContentOn: html
	html heading
		level: 3;
		with:
			'Message box for agent '
				,
					(self model
						ifNil: [ '"no agent selected"' ]
						ifNotNil: [ self model id ]).
	self model notNil
		ifTrue: [ self renderComposeButtonsOn: html.
			self renderMessagesOn: html ]