callbacks
view: aMessage
	| viewer |
	viewer := FlexibleComposer model: aMessage.
	viewer agent: self model. 
	viewer beReadOnly.
	self call: viewer