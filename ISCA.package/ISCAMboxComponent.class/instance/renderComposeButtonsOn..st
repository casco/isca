rendering
renderComposeButtonsOn: html
	html anchor
		callback: [ self composeCallForProposals ];
		with: [ html tbsButton
				beExtraSmall;
				with: 'Compose a CFP' ]