rendering
renderActionButtonsForCallForProposal: msg on: html
	msg sender = self model url
		ifFalse: [ html anchor
				callback: [ self refuseFor: msg ];
				with: [ html tbsButton
						beExtraSmall;
						with: 'refuse' ].
			html space.
			html anchor
				callback: [ self proposeFor: msg ];
				with: [ html tbsButton
						beExtraSmall;
						with: 'propose' ] ]