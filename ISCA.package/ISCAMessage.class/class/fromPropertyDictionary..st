instance creation
fromPropertyDictionary: dict
	^ self new
		conversationId: (dict at: 'conversationId' ifAbsent: [Time millisecondClockValue ]);
      performative: (dict at: 'performative');
		sender: (dict at: 'sender');
		receiver: (dict at: 'receiver');
		inReplyTo: (dict at: 'inReplyTo' ifAbsent: [ nil ]);
		replyWith: (dict at: 'replyWith' ifAbsent: [ nil ]);
		content: (dict at: 'content' ifAbsent: [ nil ]);
		yourself