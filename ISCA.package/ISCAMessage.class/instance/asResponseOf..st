responding
asResponseOf: aMsg
	sender := aMsg receiver.
	receiver := aMsg sender.
	conversationId := aMsg conversationId.
	inReplyTo := aMsg replyWith