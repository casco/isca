converting
asPropertyDictionary
	^ Dictionary new
		at: 'performative' put: self className;
		at: 'conversationId' put: conversationId;
		at: 'sender' put: sender;
		at: 'receiver' put: receiver ;
		at: 'replyWith' put: replyWith ;
		at: 'inReplyTo' put: inReplyTo ;
		at: 'content' put: content;
		yourself