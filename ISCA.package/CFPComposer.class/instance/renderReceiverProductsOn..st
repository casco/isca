rendering
renderReceiverProductsOn: html
	html
		label: 'Products by this agent';
		break.
	html
		paragraph: [ receiverProducts
				ifEmpty: [ html text: 'refresh to find out' ]
				ifNotEmpty: [ receiverProducts
						do: [ :each | 
							html anchor
								callback: [ item := each ];
								with: each ]
						separatedBy: [ html text: ', ' ] ].
			html space.
			html tbsButton
				callback: [ self refreshReceiverProducts ];
				beExtraSmall;
				with: [ html tbsGlyphIcon iconRefresh ] ]