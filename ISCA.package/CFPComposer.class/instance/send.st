controlling
send
	item isEmpty | count isEmpty
		ifTrue: [ ^ self errorMessage: 'Both item and count must be non-black ' ].
	[ count asNumber > 0
		ifFalse: [ ^ self errorMessage: 'Count must be more than 0' ] ]
		on: Error
		do: [ ^ self errorMessage: 'Count must be a valid number ' ].
	self model content at: 'item' put: item.
	self model content at: 'count' put: count.
	super send