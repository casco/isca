rendering
renderRequestDetailsOn: html
	html
		tbsFormGroup: [ html label
				for: 'item';
				with: 'Item'.
			html break.
			html textInput
				on: #item of: self;
				tbsFormControl;
				id: 'item';
				placeholder: 'the item to request' ].
	html
		tbsFormGroup: [ html label
				for: 'count';
				with: 'Count'.
			html break.
			html textInput
				on: #count of: self;
				tbsFormControl;
				id: 'count';
				placeholder: 'number of units to request' ]