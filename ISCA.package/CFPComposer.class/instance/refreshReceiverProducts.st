controlling
refreshReceiverProducts
	self model receiver
		ifNotEmpty: [ | api baseUrlAndAgentId receiverAgent |
			baseUrlAndAgentId := self model receiver splitOn: '/agents/'.
			api := ServerlessEndpointAPI baseUrl: baseUrlAndAgentId first.
			receiverAgent := api agent: baseUrlAndAgentId second asNumber .
			receiverProducts := receiverAgent products keys]