rendering
renderNewPropertyInputFormOn: html
	^ html
		tableData: [ html textInput
				on: #newPropertyName of: self ];
		tableData: [ html textInput
				on: #newPropertyValue of: self;
				class: 'col-md-12' ];
		tableData: [ html tbsButton
				beExtraSmall;
				callback: [ self addNewProperty ];
				with: 'Add' ]