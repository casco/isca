rendering
renderContentOn: html
	html label: 'Message content'.
	html
		tbsTable: [ html
				tableHead: [ html tableHeading
						class: 'col-md-2';
						with: 'Property'.
					html tableHeading
						class: 'col-md-9';
						with: 'Value'.
					html tableHeading
						class: 'col-md-1';
						with: 'Action'.
					html
						tableBody: [ (self model content ifNil: [ Dictionary new ])
								associationsDo: [ :ass | 
									html
										tableRow: [ html
												tableData: ass key;
												tableData: ass value;
												tableData: [ beReadOnly
														ifFalse: [ html tbsButton
																beExtraSmall;
																callback: [ self removeProperty: ass key ];
																with: 'Remove' ] ] ] ].
							beReadOnly
								ifFalse: [ html tableRow: [ self renderNewPropertyInputFormOn: html ] ] ] ] ]