responding
asProposeTemplate
	^ Propose new
		asResponseOf: self;
		yourself