responding
asAcceptProposalTemplate
	^ AcceptProposal new
		asResponseOf: self;
		content: self content;
		yourself