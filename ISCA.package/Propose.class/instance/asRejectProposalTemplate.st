responding
asRejectProposalTemplate
	^ RejectProposal new
		asResponseOf: self;
		content: self content;
		yourself