replying
asRefuseTemplate
	^ Refuse new
		asResponseOf: self;
		content: self content;
		yourself