replying
asProposeTemplate
	^ Propose new
		asResponseOf: self;
		content: self content;
		yourself