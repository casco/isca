instances
agentId: anId
	^ self agentsDictionary at: anId ifAbsent: [ nil ]