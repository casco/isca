instances
agentId: anId  url: url
	^ self agentsDictionary
		at: anId
		ifAbsent: [ agents
				at: anId
				put:
					(ISCAAgent new
						id: anId;
						url: url;
						yourself) ]