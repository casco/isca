instances
newAgentWithRandomIdAtBase: baseUrl
	| ids newId |
	ids := agents keys.
	[ newId := ids size + 1.
	ids includes: newId printString ] whileTrue: [ newId := newId + 1 ].
	^ self agentId: newId printString url: baseUrl , newId printString