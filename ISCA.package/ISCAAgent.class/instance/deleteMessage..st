accessing
deleteMessage: aMsg
	| conversation |
	conversation := conversations detect: [ :each | each key = aMsg conversationId ].
	conversation value remove: aMsg.
	conversation value ifEmpty: [ conversations remove: conversation ]