messages
newCallForProposals
	^ CallForProposal new
		sender: self url;
		conversationId: self nextEventId