messages
send: aMessage
	| entity |
	aMessage replyWith: 'rw-' , self nextEventId printString.
	self addMesage: aMessage.
	entity := ZnStringEntity text: aMessage asJson.
	ZnClient new
		url: aMessage receiver , '/messages';
		entity: entity;
		post