accessing
nextEventId
	NextEventId ifNil: [ NextEventId := 0 ].
	NextEventId := NextEventId + 1.
	^ NextEventId