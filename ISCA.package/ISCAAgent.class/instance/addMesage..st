messages
addMesage: aMessage
	(conversations
		detect: [ :each | each key = aMessage conversationId ]
		ifNone: [ conversations add: aMessage conversationId -> OrderedCollection new ]) value
		add: aMessage