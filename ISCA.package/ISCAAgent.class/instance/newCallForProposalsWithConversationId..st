messages
newCallForProposalsWithConversationId: cid
	^ CallForProposal new
		sender: self url;
		conversationId: cid