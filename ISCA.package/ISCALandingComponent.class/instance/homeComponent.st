hooks
homeComponent
	| inbox |
	inbox := ISCAMboxComponent new.
	inbox model: (selectedAgent ifNil: [ ISCAAgent newAgentWithRandomIdAtBase: ISCAMboxComponent ServerURL ]).
	^ inbox