overrides
populateAgentSelectorOn: html
	html
		tbsFormGroup: [ html label: 'Select an agent'.
			html div
				tbsDropdown: [ html tbsDropdownAnchor
						with: [ html
								text:
									(selectedAgent ifNil: [ 'Create new' ] ifNotNil: [ selectedAgent url ]);
								tbsCaret ].
					html
						tbsDropdownMenu: [ ISCAAgent agents
								do: [ :each | 
									html
										tbsDropdownMenuItem: [ html anchor
												callback: [ selectedAgent := each ];
												with: each url ] ].
							html tbsDivider.
							html
								tbsDropdownMenuItem: [ html anchor
										callback: [ selectedAgent := nil ];
										with: 'Create new' ] ] ] ]