overrides
renderMainContentOn: html
	html
		tbsJumbotron: [ html heading: self applicationName.
			html tbsForm
				with: [ self populateAgentSelectorOn: html.
					html
						tbsFormGroup: [ html label: 'Provide your access code'.
							html textInput
								on: #accessCode of: self;
								tbsFormControl;
								id: 'accessCode';
								placeholder: 'your access code' ].
					html space.
					html tbsButton
						callback: [ self grantAccessOnValidCode ];
						with: 'Submit' ] ]