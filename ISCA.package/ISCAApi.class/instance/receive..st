request handling
receive: agentId
	<post>
	<path: '/agents/{agentId}/messages'>
	| body |
	body := self requestContext request rawBody ifEmpty: [ self requestContext request postFields keys first] .
	(ISCAAgent agentId: agentId) receive: (self buildMessageFrom: body).
	^ 'OK'