request handling
buildMessageFrom: aString
	| props messageClass |
	props := NeoJSONReader fromString: aString.
	messageClass := Smalltalk at: (props at: 'performative') asSymbol ifAbsent: ISCAMessage.
	^ messageClass fromPropertyDictionary: props