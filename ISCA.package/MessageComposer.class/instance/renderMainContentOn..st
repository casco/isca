rendering
renderMainContentOn: html
	html heading
		level: 2;
		with: 'Composing a ' , self model class name.
	html tbsForm
		with: [ html
				tbsFormGroup: [ self renderFromAndToFieldsOn: html.
					"self renderConversationDetailsOn: html."
					self renderMessageContentOn: html.
					readOnly
						ifFalse: [ self renderSubmitButtonsOn: html ] ] ].
	self model markAsRead