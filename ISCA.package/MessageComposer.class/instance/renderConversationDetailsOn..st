rendering
renderConversationDetailsOn: html
	html label: 'Conversation id'.
	html paragraph: (self model conversationId ifNil: [ 'not set' ]).
	html label: 'In reply to'.
	html paragraph: (self model inReplyTo ifNil: [ 'not set' ]).
	html label: 'Reply with'.
	html paragraph: (self model replyWith ifNil: [ 'not set' ])