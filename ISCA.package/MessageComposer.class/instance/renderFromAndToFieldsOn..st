rendering
renderFromAndToFieldsOn: html
	html label: 'From'.
	html paragraph: self model sender.
	html
		tbsFormGroup: [ html label
				for: 'to';
				with: 'To'.
				html break.
			readOnly
				ifTrue: [ html text: self model receiver ]
				ifFalse: [ html textInput
						on: #receiver of: self model;
						tbsFormControl;
						id: 'to';
						placeholder: 'http://[domain]/agents/[agent-id]' ] ]