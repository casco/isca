rendering
renderSubmitButtonsOn: html
	html tbsButton
		callback: [ self answer: nil ];
		with: 'Cancel'.
	html space.
	^ html tbsButton
		callback: [ self send];
		with: 'Send'