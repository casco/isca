callbacks
send
	self model receiver isEmpty
		ifTrue: [ ^ self errorMessage: 'Receiver cannot be empty ' ].
	agent send: self model.
	self answer: nil