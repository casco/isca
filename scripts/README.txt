To install 

#clean files  from previous install
rm -Rf pharo-local
rm isca.*

#copy a clean image
./pharo pharo.image save isca  
	
#install from the repository
./pharo isca.image -st install.st --save

#start the server
./pharo isca.image -st run.st --no-quit

#update
./pharo isca.image -st install.st --save
